<?php

	//again, change this namespace:
	namespace MapColumn;
	
	use ChefSections\Columns\DefaultColumn;
	use Cuisine\Wrappers\Field;
	use Cuisine\Utilities\Url;
	use Cuisine\Wrappers\Script;
	
	
	class Column extends DefaultColumn{
	
		/**
		 * The type of column
		 * 
		 * @var String
		 */
		public $type = 'map';
	
	
		/*=============================================================*/
		/**             Template                                       */
		/*=============================================================*/
	
	
		/**
		 * Start the column template
		 * 
		 * @return string ( html, echoed )
		 */
		public function beforeTemplate(){
			$mapkey = apply_filters( 'chef_map_column_mapkey','AIzaSyAQk_m9Jjz68ro3tZCvuzg8YalWaH-8d8I' );
			$language = apply_filters( 'chef_map_column_maplanguage','nl' );
			$region = apply_filters( 'chef_map_column_mapregion','NL' );
			//runs before Assets/template.php is presented
			$mapsUrl = '//maps.googleapis.com/maps/api/js?key='.$mapkey.'&language='.$language.'&region='.$region.'&sensor=false';
			$scriptUrl = Url::plugin( 'chef-map-column', true ).'Assets/js/';

			echo '<script src="'.$mapsUrl.'"></script>';
			
			Script::register( 'map-column-script', $scriptUrl.'script', true );

			$this->setLocationsVar();

		}
	
	
	
		/**
		 * Add the javascript vars for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function setLocationsVar(){
	
			//get the required vars:
			
			//marker			
			$marker = Url::plugin('chef-map-column/Assets/images/marker.png');
			if ( file_exists( Url::path('theme','/images/marker.png' ) ) )
				$marker = Url::theme('images/marker.png');

			//lat lng
			$latlng = $this->getField('latlng');
			
			//infowindow
			$infowindow = '';

			//show controls
			$controls = $this->getField( 'showcontrols' );

			if( $controls == 'true' ){

				$controls = true;

			}else {

				$controls = false;

			}

			//show infowindow
			$showinfowindow = $this->getField( 'showinfowindow' );

			if( $showinfowindow == 'true' ){

				$infowindow = $this->getField( 'content' );

			} else {

				$showinfowindow = false;

			}

			//set variables object:
			$vars = array(

				'locations' => array(

						'address' 		=> $this->getField( 'address'),
						'lat'			=> $latlng['lat'],
						'lng'			=> $latlng['lng'],
						'infowindow'	=> $infowindow
				),

				'showcontrols' 			=> $controls,
				'showinfowindow' 		=> $showinfowindow,
				'icon'					=> $marker,
				'zoomlevel'				=> (int)$this->getField( 'zoomlevel' )
			);

			Script::variable( 'Locations', $vars );
			
		}
	
	
		/*=============================================================*/
		/**             Backend                                        */
		/*=============================================================*/
	
	
		/**
		 * Create the preview for this column
		 * 
		 * @return string (html,echoed)
		 */
		public function buildPreview(){
				
			echo '<strong>'.$this->getField( 'title' ).'</strong>';
			echo '<p>'.$this->getField( 'address' ).'</p>';
			echo '<p>'.$this->getField( 'region' ).'</p>';
			echo '<p>'.$this->getField( 'showinfowindow' ).'</p>';
			echo '<img src="http://maps.googleapis.com/maps/api/staticmap?center='.$this->getField( 'latlng' )['lat'].','.$this->getField( 'latlng' )['lng'].'&zoom=20&scale=false&size=200x200&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7Ccolor:red%7C'.$this->getField( 'latlng' )['lat'].','.$this->getField( 'latlng' )['lng'].'&markers=size:mid%7Ccolor:red%7Clabel:1%7C'.$this->getField( 'address' ).'"" alt="">';
	
		}


		/**
		 * Save the properties of this column
		 * 
		 * @return bool
		 */
		public function saveProperties(){

			global $post;
			$props = $_POST['properties'];


			if (isset( $props['address']) ){

				if ($props['address'] != $this->getField('address')) {
					if ($props['region'])
						$props['latlng'] = $this->getLatLng( $props['address'], $props['region'] );
					else
						$props['latlng'] = $this->getLatLng( $props['address'] );
				}else {
					$props['latlng'] = $this->getField( 'latlng' );
				}
			}

			if( !isset( $props['showcontrols'] ) )
				$props['showcontrols'] = 0;

			if( !isset( $props['showinfowindow'] ) )
				$props['showinfowindow'] = 0;

			$saved = update_post_meta( 
				$post->ID, 
				'_column_props_'.$this->fullId, 
				$props
			);

			//set the new properties in this class
			$this->properties = $props;
			return $saved;

		}
	
	
		/**
		 * Build the contents of the lightbox for this column
		 * 
		 * @return string ( html, echoed )
		 */
		public function buildLightbox(){
	
			//get all fields for this column
			$fields = $this->getFields();
	
			echo '<div class="main-content">';
			
				foreach( $fields as $field ){
	
					$field->render();
	
					//if a field has a JS-template, we need to render it:
					if( method_exists( $field, 'renderTemplate' ) ){
						echo $field->renderTemplate();
					}
	
				}
	
			echo '</div>';
			echo '<div class="side-content">';
				
				//optional: side fields
	
				$this->saveButton();
	
			echo '</div>';
		}
	
	
		/**
		 * Get the fields for this column
		 * 
		 * @return Array (all fields and it's defaultvalues in an array)
		 */
		private function getFields(){
	
			$fields = array(
				'title' => Field::text( 
					'title', 
					'',
					array(
						'label' 				=> false,
						'placeholder' 			=> 'Titel',
						'defaultValue'			=> $this->getField( 'title' ),
					)
				),
				'address' => Field::text( 
					'address', //this needs a unique id 
					'', 
					array(
						'label'				=> false,
						'placeholder'		=> 'Adres',
						'defaultValue' 		=> $this->getField( 'address' )
					)
				),
				'region' => Field::text( 
					'region', //this needs a unique id 
					'', 
					array(
						'label'				=> false,
						'placeholder'		=> 'Regio (default - Nederland)',
						'defaultValue' 		=> $this->getField( 'region' )
					)
				),
				'zoomlevel' => Field::number( 
					'zoomlevel', //this needs a unique id 
					'', 
					array(
						'label'				=> 'Zoom level',
						'placeholder'		=> 'Zoom level',
						'defaultValue' 		=> $this->getField( 'zoomlevel', 16 )
					)
				),
				'content' => Field::editor( 
					'content', //this needs a unique id 
					'Informatie popup', 
					array(
						'defaultValue' 		=> $this->getField( 'content' ),
						'id'				=> $this->fullId
					)
				),
				'showcontrols' => Field::checkbox( 
					'showcontrols', //this needs a unique id 
					'Toon mapcontrols', 
					array(
						'defaultValue' 		=> $this->getField( 'showcontrols', false ),
						'class'				=> array( 'input-field', 'field-showcontrols', 'type-checkbox', 'subfield' )
					)
				),
				'showinfowindow' => Field::checkbox( 
					'showinfowindow', //this needs a unique id 
					'Toon infowindow', 
					array(
						'defaultValue' 		=> $this->getField( 'showinfowindow', false ),
						'class'				=> array( 'input-field', 'field-showinfowindow', 'type-checkbox', 'subfield' )
					)
				)

			);

			return $fields;
	
		}


		/**
		* Get the Latitude and Longitude of a address
		*
		* @return Array (lat and long) 
		*/
		private function getLatLng( $address, $region = 'Nederland' ){

			$address = str_replace( ' ', '+', $address );
			$region = str_replace( ' ', '+', $region ); 

			$json = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false&region='.$region);
			$json = json_decode($json);

			$latlng = Array (
				"lat" => 0,
				"lng" => 0
			);

			try {
			    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
				$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};

				$latlng = Array (
					"lat" => $lat,
					"lng" => $long
				);
			} catch (Exception $e) {
			    // TODO: Add exceptionhandling here!

			} 

			return $latlng;
		}
	
	}
	