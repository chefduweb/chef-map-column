define([
	
	//define dependencies at the top:
	'jquery'

], function( $ ){

	$(document).ready(function(){


		// add a map reference
		var map = {};

		// add markers reference
		var markers = [];

		// map properties
	    var mapProp = {
	        zoom        : Locations.zoomlevel,
	        scrollwheel: Locations.showcontrols,
	        navigationControl: Locations.showcontrols,
	        mapTypeControl: Locations.showcontrols,
	        scaleControl: Locations.showcontrols,
	        draggable: Locations.showcontrols,
	        disableDefaultUI: !Locations.showcontrols,
	        mapTypeId   : google.maps.MapTypeId.ROADMAP
	    };

	    // initialize map
		function initialize() {
			map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

			addMarkers($(this)[0]);
			
		}

		// add markers to map and set the bounds
		function addMarkers() {
			// create bounds object
			var bounds = new google.maps.LatLngBounds();

			if (Locations && Locations.locations) {

				$(Locations.locations).each(function(){
					
					// create latlng object
					var latLng = new google.maps.LatLng( $(this)[0].lat, $(this)[0].lng );

					// create infowindow
					var infowindow = null;

					if($(this)[0].infowindow && $(this)[0].infowindow != null) {
						infowindow = new google.maps.InfoWindow({content: $(this)[0].infowindow});
					}

					// create marker
				    var marker = new google.maps.Marker({
				        position    : latLng,
				        map 		: map,
				        infoWindow  : infowindow,
				        icon 		: Locations.icon
				    });

				    markers.push( marker );

				    // add eventlistener to marker
				    google.maps.event.addListener( marker, 'click', function() {

	    				clearWindows();
	    				if (this.infoWindow) {
	    					this.infoWindow.open( map, this );
	    				}

	  				});

				    // extend bounds with marker
				    bounds.extend( latLng );

				});
				// check if there is only one marker, if so, open the infowindow
				if (markers.length == 1) {
					if (markers[0].infoWindow && Locations.showinfowindow) {
						markers[0].infoWindow.open( map, markers[0]);
					}
					map.setCenter(markers[0].getPosition());
				} else {
					// set map center
					map.fitBounds( bounds );
				}
			} else {
				// set default mapcenter
				var latLng = new google.maps.LatLng( 51.768950, 5.524325);
				map.setCenter(latLng);
			}
		}	

		/**
		 * Remove info windows on the map
		 * @return void
		 */
		function clearWindows(){

			for( var i = 0; i < markers.length; i++ ){
				
				if (markers[i].infoWindow) {
					markers[i].infoWindow.close();
				}

			}
		}

		// initialize map on document load
		initialize();

	});

});