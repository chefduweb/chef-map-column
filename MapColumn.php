<?php
/**
 * Plugin Name: Chef Map Column
 * Plugin URI: http://chefduweb.nl/plugins/column-map
 * Description: Default Column started kit
 * Version: 1.0.3
 * Author: Remy Bakker
 * Author URI: http://www.chefduweb.nl/
 * License: GPLv2
 * Bitbucket Plugin URI: https://bitbucket.org/chefduweb/chef-map-column
 * Bitbucket Branch:     master
 * 
 * @package Cuisine
 * @category Core
 * @author Chef du Web
 */

//Chaning the namespaces is the most important part, 
//after that the bus pretty much drives itself.
namespace MapColumn;

use Cuisine\Wrappers\Script;
use Cuisine\Wrappers\Sass;
use Cuisine\Utilities\Url;


class ColumnIgniter{ 

	/**
	 * Static bootstrapped MapColumn\ColumnIgniter instance.
	 *
	 * @var \MapColumn\ColumnIgniter
	 */
	public static $instance = null;


	/**
	 * Init admin events & vars
	 */
	function __construct(){

		//register column:
		$this->register();

		//load the right files
		$this->load();

		//assets:
		$this->enqueues();


	}


	/**
	 * Register this column-type with Chef Sections
	 * 
	 * @return void
	 */
	private function register(){


		add_filter( 'chef_sections_column_types', function( $types ){

			$base = Url::path( 'plugin', 'chef-map-column', true );

			//change the $types[ key ] and the name value:
			$types['map'] = array(
						'name'		=> 'Map',
						'class'		=> 'MapColumn\Column',
						'template'	=> $base.'Assets/template.php'
			);

			return $types;

		});

	}

	/**
	 * Load all includes for this plugin
	 * 
	 * @return void
	 */
	private function load(){

		//only if Chef Sections is loaded as well:
		add_action( 'chef_sections_loaded', function(){

			include( 'Classes/Column.php' );

		});

	}


	/**
	 * Enqueue scripts & Styles
	 * 
	 * @return void
	 */
	private function enqueues(){

		add_action( 'init', function(){

			//javascript files for front-end use:
			
			//$url = Url::plugin( 'chef-map-column', true ).'Assets/js/';
			//Script::register( 'google-map-api', "http://maps.googleapis.com/maps/api/js?sensor=false", false );
			

			//sass files for front-end use:
			
			//$url = 'chef-default-column/Assets/sass/';
			//Sass::register( 'column-sass', $url.'_style.scss', false );
		
		});
	}

	/*=============================================================*/
	/**             Getters & Setters                              */
	/*=============================================================*/


	/**
	 * Init the \MapColumn\ColumnIgniter Class
	 *
	 * @return \MapColumn\ColumnIgniter
	 */
	public static function getInstance(){
		
	    return static::$instance = new static();

	}


}

\MapColumn\ColumnIgniter::getInstance();